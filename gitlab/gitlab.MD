MENÚ
- [Git lab](#git-lab)
  - [¿Qué es gitlab?](#qué-es-gitlab)
    - [Características principales](#características-principales)
    - [¿Cómo se accede?](#cómo-se-accede)
- [Crear repositorios](#crear-repositorios)
- [Crear grupos](#crear-grupos)
- [Crear Subgrupos](#crear-subgrupos)
- [Crear issues](#crear-issues)
- [Crear labels](#crear-labels)
- [Permisos en gitlab y para qué sirven.](#permisos-en-gitlab-y-para-qué-sirven)
- [Agregar miembros](#agregar-miembros)
- [Crear boards y manejo de boards](#crear-boards-y-manejo-de-boards)
- [Commit desde gitlab y crear ramas desde gitlab](#commit-desde-gitlab-y-crear-ramas-desde-gitlab)
  - [Crear ramas](#crear-ramas)
  - [Hacer commits](#hacer-commits)

# Git lab
## ¿Qué es gitlab?
Esta herramienta permite evidenciar los niveles de eficiencia de las aplicaciones que se desarrollan bajo este concepto. De esta manera podemos conllevar a que nuestro ciclo de desarrollo de software tenga una notoria mejoría y una rapidez lo que permitirá entregar un producto de forma eficaz y organizada.

### Características principales 
- Manejo de código fuente
- Revisión de código
- Seguimiento de incidencias 
- Tablero de incidencias 
- Integración continua 
- Repositorio Maven
- Métricas

### ¿Cómo se accede? 
Para acceder a esta herramienta, debes Abrir en el navegador web el siguiente enlace: [gitlab.com](https://about.gitlab.com/) y despues tendras que registrate como nuevo usuario y ya tendras acceso a todos sus funcionamientos.   

# Crear repositorios
Para crear repositorios se debe seguir los siguientes pasos: 

1. Para crear un repositorio iremos a la parde que dice _Projects_ y en _Personal_ y daremos a _New project_. 

![Repositorio1](capturas/repositorio-gitlab.png)

1. Ahora le daremos a la opcion de _Create a blank project_.

![Repositorio2](capturas/repositorio-gitlab%202.png)

1. Una dentro, vamos a llenar todos los campos. Tendremos la opcion de poner ese repositorio en publico o privado. Y lo mas importate es empezar siempre un repositorio con el archivo README, asi que marcaremos esa casilla. 

![Repositorio3](capturas/repositorio-gitlab%203.png)

4. Ya estaría creado nuestro repositorio y listo para seguir con nuestro trabajo.
   
![Repostirio4](capturas/repositorio-gitlab%204.png)

# Crear grupos
Para crear grupos debemos seguir los siguientes pasos: 

1. Vamos a ir a la esquina superior izquierda y daremos a _Groups_, se desplegara unas cuantas opciones y solo daremos clic en _Your groups_.

![Grupos1](capturas/grupos-gitlab.png)

2. Ahora, vamos a darle en le bontón _New group_

![Grupo2](capturas/grupos-gitlab%202.png)

3. Una vez dentro, llenaremos los campos que sean necesarios. La opcion de agregar a un miembro es opcional. 

![Grupo3](capturas/grupos-gitlab%203.png)

4. Ya tendriamos completado la creación de un grupo en GitLab.

![Grupo4](capturas/grupos-gitlab%204.png)

# Crear Subgrupos
Para crear subgrupos lo haremos en el grupo que ya creamos anteriormente, y para ellos seguiremos los siguientes pasos: 

1. Una vez dentro del grupo que hemos creado o de cualquier otro grupo, vamos a dar en el botón de _New subgroup_.

![Subgrupo1](capturas/subgrupo-gitlab.png)

2. Llenaremos los campos que sean necesarios

![Subgrupo2](capturas/subgrupo-gitlab%202.png)

3. Ya estaria creado el subgrupo y lo visualizaremos de primer plano. Para verificar que se haya creado bien el subgrupo regresaremos al grupo que hemos creado.

![Subgrupo3](capturas/subgrupo-gitlab%203.png)

4. Y ahora veremos que estaria el grupo principal y el subgrupo que hemos creado. 

![Subgrupo4](capturas/subgrupo-gitlab%204.png)

# Crear issues
Para crear issues vamos a utilizar el repositorio que habaiamos creado anteriormente o podemos ocupar otro repositorio, para ellos necesitamos seguir lo siguientes pasos:

1. Estando dentro de nuestro repositorio, en el lado izquierdo tendremos varias opciones, pero solo escogeremos la opcion de _Issue_.

![Issue1](capturas/issue-gitlab.png)

2. Le daremos en el botón _New Issue_.

![Issue2](capturas/issue-gitlab%202.png)

3. Vamos a llenar por el momento solo los campos de el nombre y una descripcion que le queramos poner a nuestro issue. Esta descripcion la podremos editar mas adelante. 
   
![Issue3](capturas/issue-gitlab%203.png)

4. Ya tendriamos creado nuestro primer issue.
   
![Issue4](capturas/issue-gitlab%204.png)

# Crear labels
Para crear labels ocuparemos el repositorio que hemos creado anteriormente u otro repositorio, seguiremos lo siguientes pasos:

1. Nos vamos a dirigir a la opcion de _issues_, esperamos a que se despliegue la barra y escogeremos la opcion de _labels_.

![label1](capturas/label-gitlab.png)

2. Vamos a darle a la opcion de _New label_.
   
![label2](capturas/label-gitlab%202.png)

3. Una vez dentro vamos a llenar los campos y escogeremos el color que queramos. Si vamos a crear mas labels lo recomendable sería escoger un color por cada label creado para que no haya confucion mas adelante. 
   
![label3](capturas/label-gitlab%203.png)

4. Y listo, ya tendremos creado el primer label. Si queremos crear mas solo daremos al botón en la esquina superior derecha y haremos el mismo procedimiento. 
   
![label4](capturas/label-gitlab%204.png)

# Permisos en gitlab y para qué sirven.
Los permisos en gitlab son las acciones que puede realizar el miembro del Repositorio, grupos, subgrupos, issues, etc. Por defecto, cuando te creas algunos de estosm, vienes como _Maintainer_ que es el mas alto de lso permiso. Si quieres agregar a mas gente tú puedes dar un rol para que esa persona pueda hacer determinadas acciones. Cada rol tiene sus limitaciones. Aquí les muestro algunos ejemplos de que puede hacer cada rol en gitlab:

**Permisos**

| Roles | Responsabilidades | Requerimientos |
| ------ | ------ | ------ | 
| Maintainer | Acepta solicitudes de fusión en varios proyectos de GitLab | Añadido a la página del equipo. Un experto en revisiones de códigos y conoce el producto/código base |
| Reviewer | Realiza revisiones de códigos en las resonancias magnéticas | Añadido a la página del equipo | 
| Developer | Tiene acceso a la infraestructura y a las cuestiones internas de GitLab (por ejemplo, relacionadas con los recursos humanos) | Empleado de GitLab o un miembro del Equipo Central (con un NDA) |
| Contributor | Puede hacer contribuciones a todos los proyectos públicos de GitLab | Tener una cuenta en GitLab.com |


**Permisos de usuarios**

| Usuario: Gest | Si | No |
| ------ | ------ | ------ |
| Crear issues | X |  |
| Editar issues | X |  |
| Cambiar etiquetas |  | X |
| Crear etiquetas |  | X |
| Eliminar issues |  | X |

--------------------------------

| Usuario: Reporter | Si | No |
| ------ | ------ | ------ |
| Crear issues | X |  |
| Editar issues | X |  |
| Cambiar etiquetas |  | X |
| Crear etiquetas |  | X |
| Eliminar issues |  | X |

---------------------------------

| Usuario: Developer | Si | No |
| ------ | ------ | ------ |
| Crear issues | X |  |
| Editar issues | X |  |
| Cambiar etiquetas | X |  |
| Crear etiquetas | X |  |
| Eliminar issues |  | X |

---------------------------------

| Usuario: Maintainer | Si | No |
| ------ | ------ | ------ |
| Crear issues | X |  |
| Editar issues | X |  |
| Cambiar etiquetas | X |  |
| Crear etiquetas | X |  |
| Eliminar issues |  | X |

# Agregar miembros
Los miembros en gitlab son importantes, pero tambien debemos saber que rol darle para que no haya inconvenientes y cómo agregarlos. Sigamos estos pasos:

1. Primero vamos a dirigirnos a un repositorio que hayamos creado anteriormente. Ahora en la parte izquierda veremos una opcion que dice _Members_ vamos a dar clic ahi.

![Miembro1](capturas/miembros-gitlab.png)

2. Una vez dentro vamos a llenar el campo de a quien vamos a querer invitar. Vamos a poner el nombre de usuario de quien queramos invitar y el rol que le va a corresponer y le daremos en el botón de _invite_.
   
![Miembro2](capturas/miembros-gitlab%202.png)

3. Ya estaria invitado la persona a la que haya agregado. Y si, se confundienron de perona o pusieron mal su rol, pueden editar el rol y borrar a esa persona. Si quieren agregar mas miembros tendrian que hacer el mismo procedimiento.

![Miembro3](capturas/miembros-gitlab%203.png)

# Crear boards y manejo de boards
Para crear boards y tener organizados nuestro tiempo de trabajo haremos lo siguiente:

1. Vamos a ir a la parte izquierda y vamos a la opcion de _issue_, esperamos a que se abra la barra de opciones y elegiremos la opcion de _board_.

![board1](capturas/boards-gitlab%201.png)

2. Una vez en board vamos a visualizar un board por defecto. Si queremos crear un board solo vamos en la parte izquierda donde dice _Development_ y le daremos a _Create new board_.

![board2](capturas/boards-gitlab%202.png)

3. Llenaremos los campos correspondientes y le daremos a _Create board_.

![board3](capturas/boards-gitlab%203.png)

4. Una vez creado ya podremos visualizar el board.
   
![board4](capturas/boards-gitlab%204.png)

5. Si queremos una mejor organizacion podemos crar labels, y al crearlo podemos unir a los boards y con ello unir issues de trabajo para tener un lugar de trabajo super organizado. Para ello iremos a la opcion de _Add list_ y agregaremos los labels que tengamos creados.

![board5](capturas/boards-gitlab%205.png)

6. Cuano hayamos agregado el label a nuestro board, podremos agregar los issues en el botón con el simbolo **+** y buscaremos el issue que queramos agregar.
   
![board6](capturas/boards-gitlab%206.png)

6. Si queremos eliminar ese label daremos en la opcion con el simobolo de una tuerca y le daremos en _Remove list_.

![board7](capturas/boards-gitlab%207.png)


# Commit desde gitlab y crear ramas desde gitlab
Para hacer commits y crear ramas desde gitlab debemos haer lo siguiente:

## Crear ramas
1. Vamos a abrir un repositorio que ya hayamos creado y vamos a ir a la parte izquierda en la opcion de _Repository_, esperamos que se abra la lista de opciones y le daremos clic en la opcion _Branches_.

![rama1](capturas/ramas-gitlab.png)

2. Una vez abierta vamos a la opcion de _New Branch_ y le daremos clic.

![rama2](capturas/ramas-gitlab%202.png)

3. Vamos a llenar el campo y le dejaremos como _master_ en la segunda opcion. Es muy importante no dejar espacios cuando vayamos a darle un nombre a la rama. Lo mejor es que este seperado por guiones medios. 

![rama3](capturas/ramas-gitlab%203.png)

4. Ya estaría creada la rama. Para verificarlo iremos nuevamente a la opcion de _Branches_ y ya visualizaremos nuestra rama creada.

![rama4](capturas/ramas-gitlab%204.png)

## Hacer commits

1. Para hacer commits en gitlab vamos a abrir un repositorio y vamos a crear un _New file_ para ello vamos a la opcion que tiene como simbolo un **+** y le daremos a _New file_.

![commit1](capturas/commit-gitlab.png)

2. Una vez ahi, vamos a darle un nombre a ese file y le daremos una descripcion. Más abajo econtraremos para llenar el commit e igual lo llenaremos de la manera mas especifica posible y despues le daremos al boton _Commit change_

![commit2](capturas/commit-gitlab%202.png)

3. Ya podremos obersvar que el file se ha creado correctamente.

![commit3](capturas/commit-gitlab%203.png)

4. Ya creado el commit vamos a verificar que se haya realaizado correctamente. Para ello vamos a ir a la parte izqueirda y le daremos a la opcion de _Commits_ y primero saldra el commit que recien nomas lo realizamos.

![commit4](capturas/commit-gitlab%204.png)